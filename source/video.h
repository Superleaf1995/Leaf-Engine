#ifndef __VIDEO_H__
#define __VIDEO_H__

#include "leaf.h"
#include "mem.h"

struct Leaf_Video {
	unsigned width;
	unsigned height;
	unsigned totalSize;
	unsigned bitDepth;
	unsigned colors;
};

struct Leaf_Video_Palette {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

struct Leaf_Video_Context {
	unsigned char * internalVideoBuffer;
	unsigned lastVideoMode;
	struct Leaf_Video * currentVideo;
};

/* Macro that expands into draw pixel routine (Non-safe, don't use unless you are 100% sure) */
#define Leaf_Video_NonSafe_DrawPixel(ctx,x,y,c) ctx->internalVideoBuffer[(ctx->currentVideo->width*y)+x] = c;

int Leaf_Video_InitByMode(struct Leaf_Video_Context * ctx, unsigned w, unsigned h, unsigned char m);
int Leaf_Video_End(struct Leaf_Video_Context * ctx);

int Leaf_Video_SetPalette(struct Leaf_Video_Context * ctx, struct Leaf_Video_Palette * pal, unsigned char size, unsigned char offs);

int Leaf_Video_DrawFillSquare(struct Leaf_Video_Context * ctx, signed x, signed y, signed size, unsigned char c);
int Leaf_Video_DrawLine(struct Leaf_Video_Context * ctx, signed sx, signed sy, signed ex, signed ey, unsigned char c);
int Leaf_Video_DrawPoly(struct Leaf_Video_Context * ctx, signed x, signed y, signed * data, unsigned size, unsigned char c);
int Leaf_Video_UpdateScreen(struct Leaf_Video_Context * ctx);
int Leaf_Video_ClearScreen(struct Leaf_Video_Context * ctx);

#endif
