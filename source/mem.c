/*
	Malloc that tracks pointers and memory consumption.
	
	If program dies you can delete all pointers with a single function
	how awesome is that?
*/

#include <stdio.h>
#include "mem.h"

/**
Initializes a list object, with set max-memory

@param[list] Pointer to memory list object
@param[max_memory] Max memory which the allocater will try to not cross
@return Status
@retval 0 Success
@retval 1 Could not allocate memory for entries pointer array
*/
int Leaf_Mem_Init(struct Leaf_Mem_MemoryList * list, size_t max_memory) {
	list->max_memory = max_memory;
	list->used_memory = (sizeof(struct Leaf_Mem_MemoryList)+1);
	list->n_entries = 0;
	list->entries = malloc(1);
	if(list->entries == NULL) {
		return 1;
	}
	return 0;
}

/**
Allocate memory block

@param[list] Pointer to memory list object
@param[size] Size of new block
@return Pointer to new memory block
*/
void * Leaf_Mem_Malloc(struct Leaf_Mem_MemoryList * list, size_t size) {
	struct Leaf_Mem_MemoryEntry * entry;
	
	/* If we reached max memory or size is zero. Abort */
	if(list->used_memory+size > list->max_memory || size == 0) {
		return NULL;
	}
	
	list->entries = realloc(list->entries,sizeof(struct Leaf_Mem_MemoryEntry)*(list->n_entries+1));
	if(list->entries == NULL) {
		return NULL;
	}
	
	/* Allocate object */
	list->entries[list->n_entries].ptr = malloc(size);
	if(list->entries[list->n_entries].ptr == NULL) {
		return NULL;
	}
	list->entries[list->n_entries].size = size;
	
	/* Once everything goes well, add to list of entries */
	list->n_entries++;
	list->used_memory += size+sizeof(struct Leaf_Mem_MemoryEntry);
	return list->entries[list->n_entries-1].ptr;
}

/**
Reallocate a memory entry block

@param[list] Pointer to memory list object
@param[pointer] Pointer of block that needs to be resized
@param[size] New size of block
@return Pointer to new block
*/
void * Leaf_Mem_Realloc(struct Leaf_Mem_MemoryList * list, void * pointer, size_t size) {
	size_t i;
	for(i = 0; i < list->n_entries; i++) {
		if(list->entries[i].ptr != pointer) {
			continue;
		}
		
		/* Reallocate, and calculate used memory accordingly */
		list->entries[i].ptr = realloc(list->entries[i].ptr,size);
		if(list->entries[i].ptr == NULL || list->used_memory+size > list->max_memory || size == 0) {
			memmove(&list->entries[i],&list->entries[i+1],sizeof(struct Leaf_Mem_MemoryEntry)*(list->n_entries-i-1));
			return NULL;
		}
		list->used_memory += (size-list->entries[i].size)+sizeof(struct Leaf_Mem_MemoryEntry);
		list->entries[i].size = size;
		return list->entries[i].ptr;
	}
	return NULL;
}

/**
Frees a single memory entry

@param[list] Pointer to memory list object
@param[pointer] Pointer of the desired memory block that needs to be deallocated
*/
void Leaf_Mem_Free(struct Leaf_Mem_MemoryList * list, void * pointer) {
	size_t i;
	for(i = 0; i < list->n_entries; i++) {
		if(list->entries[i].ptr != pointer) {
			continue;
		}
		
		/* Free the pointer */
		free(list->entries[i].ptr);
		list->used_memory -= list->entries[i].size-sizeof(struct Leaf_Mem_MemoryEntry);
		
		/* Remove from entries */
		memmove(&list->entries[i],&list->entries[i+1],sizeof(struct Leaf_Mem_MemoryEntry)*(list->n_entries-i-1));
		
		list->n_entries--;
		return;
	}
	return;
}

/**
Allocates memory in bulks, useful for allocating various blocks at the same time

@param[list] Pointer to memory list object
@param[n_entries] Number of entries to create
@param[size] Sizes for each entry (array)
@return Array of pointers, each one has pointers to a memory block
*/
void ** Leaf_Mem_MallocBulk(struct Leaf_Mem_MemoryList * list, size_t n_entries, size_t * size) {
	size_t i;
	void ** ptr;
	
	/* Allocate array for return value (return value being a dynarray) */
	ptr = malloc(sizeof(void *)*n_entries);
	if(ptr == NULL) {
		return NULL;
	}
	
	/* Pre-create entries so we save a lot of calls to realloc */
	list->entries = realloc(list->entries,sizeof(struct Leaf_Mem_MemoryEntry)*(list->n_entries+n_entries+1));
	if(list->entries == NULL) {
		return NULL;
	}
	
	/* Create entries one by one */
	for(i = 0; i < n_entries; i++) {
		list->entries[list->n_entries].ptr = malloc(size[i]);
		if(list->entries[list->n_entries].ptr == NULL) {
			return NULL;
		}
		list->entries[list->n_entries].size = size[i];
		ptr[i] = list->entries[list->n_entries].ptr;
		list->used_memory += size[i]+sizeof(struct Leaf_Mem_MemoryEntry);
		list->n_entries++;
	}
	list->used_memory += sizeof(void *)*n_entries;
	return ptr;
}

/**
Clear all memory (destroy everything; either you want to wipe out everything
or you want to end the program)

@param[list] Pointer to memory list object
*/
void Leaf_Mem_ClearAll(struct Leaf_Mem_MemoryList * list) {
	size_t i;
	for(i = 0; i < list->n_entries; i++) {
		free(list->entries[i].ptr);
	}
	free(list->entries);
	list->used_memory = (sizeof(struct Leaf_Mem_MemoryList)+1);
	list->n_entries = 0;
	return;
}

/**
Prints memory map to stdout

@param[list] Pointer to memory list object
*/
void Leaf_Mem_PrintMap(struct Leaf_Mem_MemoryList * list) {
	size_t i;
	printf("Memory used: (%u of %u)\n",list->used_memory,list->max_memory);
	for(i = 0; i < list->n_entries; i++) {
		printf("[%u] - %u block allocated at %p\n",i,list->entries[i].size,list->entries[i].ptr);
	}
	return;
}
