#ifndef __MEM_O_H__
#define __MEM_O_H__

#include <stdlib.h>

struct Leaf_Mem_MemoryEntry {
	size_t size;
	void * ptr;
};

struct Leaf_Mem_MemoryList {
	struct Leaf_Mem_MemoryEntry * entries;
	size_t used_memory;
	size_t max_memory;
	size_t n_entries;
};

int Leaf_Mem_Init(struct Leaf_Mem_MemoryList * list, size_t max_memory);
void * Leaf_Mem_Malloc(struct Leaf_Mem_MemoryList * list, size_t size);
void * Leaf_Mem_Realloc(struct Leaf_Mem_MemoryList * list, void * pointer, size_t size);
void Leaf_Mem_Free(struct Leaf_Mem_MemoryList * list, void * pointer);
void ** Leaf_Mem_MallocBulk(struct Leaf_Mem_MemoryList * list, size_t n_entries, size_t * size);
void Leaf_Mem_ClearAll(struct Leaf_Mem_MemoryList * list);
void Leaf_Mem_PrintMap(struct Leaf_Mem_MemoryList * list);

#endif
